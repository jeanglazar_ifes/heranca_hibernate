package modelo;
// Generated 13/05/2016 08:24:52 by Hibernate Tools 3.6.0


import java.util.Date;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity

public class Vendedor extends PessoaFisica implements java.io.Serializable {

     private double salario;
     
     @Temporal(TemporalType.DATE)
     private Date dtAdmissao;
     
     @Temporal(TemporalType.DATE)
     private Date dtDemissao;

    public Vendedor() {
        super();
    }

	
    public Vendedor(String nome, String cpf, double salario, Date dtAdmissao) {
        super(nome, cpf);
        this.salario = salario;
        this.dtAdmissao = dtAdmissao;
    }
    public Vendedor(String nome, String cpf, double salario, Date dtAdmissao, Date dtDemissao) {
       super(nome, cpf);
       this.salario = salario;
       this.dtAdmissao = dtAdmissao;
       this.dtDemissao = dtDemissao;
    }

    public double getSalario() {
        return salario;
    }
       
    public void setSalario(double salario) {
        this.salario = salario;
    }
    public Date getDtAdmissao() {
        return this.dtAdmissao;
    }
    
    public void setDtAdmissao(Date dtAdmissao) {
        this.dtAdmissao = dtAdmissao;
    }
    public Date getDtDemissao() {
        return this.dtDemissao;
    }
    
    public void setDtDemissao(Date dtDemissao) {
        this.dtDemissao = dtDemissao;
    }




}


