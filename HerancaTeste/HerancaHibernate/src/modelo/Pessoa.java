package modelo;
// Generated 13/05/2016 08:24:52 by Hibernate Tools 3.6.0

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;


@Entity
@Inheritance ( strategy = InheritanceType.JOINED)
public class Pessoa  implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPessoa;
    private String nome;

    public Pessoa() {
    }

    public Pessoa(String nome) {
       this.nome = nome;
    }
   
    public Integer getIdPessoa() {
        return this.idPessoa;
    }
    
    public void setIdPessoa(Integer idPessoa) {
        this.idPessoa = idPessoa;
    }
    public String getNome() {
        return this.nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }

    public Object[] toArray() {
        return new Object[] {this, ""};
    }

    @Override
    public String toString() {
        return nome;
    }

    

}


