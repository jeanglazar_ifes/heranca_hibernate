package modelo;
// Generated 13/05/2016 08:24:52 by Hibernate Tools 3.6.0

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrimaryKeyJoinColumn;


@Entity
public class PessoaFisica  extends Pessoa implements java.io.Serializable {


    private String cpf;


    public PessoaFisica() {
        super();
    }

	

    public PessoaFisica(String nome, String cpf) {
        super(nome);
        this.cpf = cpf;
    }
   
    public String getCpf() {
        return this.cpf;
    }
    
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    
    public Object[] toArray() {
        return new Object[] {this, cpf};
    }

}


