package modelo;
// Generated 13/05/2016 08:24:52 by Hibernate Tools 3.6.0

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;


@Entity
public class PessoaJuridica extends Pessoa implements java.io.Serializable {


     private String cnpj;

    public PessoaJuridica() {
        super();
    }

    public PessoaJuridica(String nome, String cnpj) {
       super(nome);
       this.cnpj = cnpj;
    }
   
    public String getCnpj() {
        return this.cnpj;
    }
    
    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public Object[] toArray() {
        return new Object[] {this, cnpj};
    }


}


