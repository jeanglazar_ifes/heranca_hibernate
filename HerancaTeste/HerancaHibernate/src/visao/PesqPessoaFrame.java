package visao;


import controlador.Controlador;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

import modelo.*;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ClientePesqFrame.java
 *
 * Created on 05/09/2013, 14:30:27
 */
/**
 *
 * @author 1547816
 */
public class PesqPessoaFrame extends javax.swing.JDialog {
    
    Controlador control;
    Pessoa pesSelecionado;
    
    /** Creates new form ClientePesqFrame */
    public PesqPessoaFrame(java.awt.Frame parent, boolean modal) throws Exception, SQLException {
        super(parent, modal);
        initComponents();
        
        control = new Controlador();
        pesSelecionado = null;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupoTipo = new javax.swing.ButtonGroup();
        txtPesq = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPessoas = new javax.swing.JTable();
        btnPesquisar = new javax.swing.JButton();
        btnSelecionar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        rdbPesFis = new javax.swing.JRadioButton();
        rdbPesJur = new javax.swing.JRadioButton();
        rdbVendedor = new javax.swing.JRadioButton();
        rdbTodos = new javax.swing.JRadioButton();

        setTitle("Pesquisar Clientes");

        tblPessoas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "CPF/CNPJ"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblPessoas);

        btnPesquisar.setText("Pesquisar");
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });

        btnSelecionar.setText("Selecionar");
        btnSelecionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelecionarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnExcluir.setText("Excluir");
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });

        jLabel1.setText("Nome");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Tipo"));

        grupoTipo.add(rdbPesFis);
        rdbPesFis.setMnemonic('F');
        rdbPesFis.setText("Pessoa Física");
        rdbPesFis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbTipoActionPerformed(evt);
            }
        });
        jPanel1.add(rdbPesFis);

        grupoTipo.add(rdbPesJur);
        rdbPesJur.setMnemonic('J');
        rdbPesJur.setText("Pessoa Jurídica");
        rdbPesJur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbTipoActionPerformed(evt);
            }
        });
        jPanel1.add(rdbPesJur);

        grupoTipo.add(rdbVendedor);
        rdbVendedor.setMnemonic('V');
        rdbVendedor.setText("Vendedor");
        rdbVendedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbTipoActionPerformed(evt);
            }
        });
        jPanel1.add(rdbVendedor);

        grupoTipo.add(rdbTodos);
        rdbTodos.setMnemonic('T');
        rdbTodos.setSelected(true);
        rdbTodos.setText("Todos");
        rdbTodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbTipoActionPerformed(evt);
            }
        });
        jPanel1.add(rdbTodos);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(75, 75, 75)
                        .addComponent(btnSelecionar)
                        .addGap(58, 58, 58)
                        .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(54, 54, 54)
                        .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 503, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(25, 25, 25)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtPesq, javax.swing.GroupLayout.PREFERRED_SIZE, 366, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnPesquisar))
                                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 426, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPesq, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPesquisar)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 283, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancelar)
                    .addComponent(btnSelecionar)
                    .addComponent(btnExcluir))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSelecionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelecionarActionPerformed
        // TODO add your handling code here:
        
        int linha = tblPessoas.getSelectedRow();
        if ( linha >= 0) {
            pesSelecionado = (Pessoa) tblPessoas.getValueAt(linha,0);    
            this.setVisible(false);
        } else {
            JOptionPane.showMessageDialog(this, "Selecione uma PESSOA.");
        }        
        
    }//GEN-LAST:event_btnSelecionarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        pesSelecionado = null;
        this.setVisible(false);
    
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed
            control.pesquisarPessoa(tblPessoas, txtPesq.getText(), grupoTipo.getSelection().getMnemonic() );
        
    }//GEN-LAST:event_btnPesquisarActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        // TODO add your handling code here:
        int linha = tblPessoas.getSelectedRow();
        if ( linha >= 0) {
            pesSelecionado = (Pessoa) tblPessoas.getValueAt(linha,0);
            try {
                control.excluirPessoa(pesSelecionado, tblPessoas);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, "ERRO ao excluir PESSOA.");
                Logger.getLogger(PesqPessoaFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Selecione uma PESSOA.");
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void rdbTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbTipoActionPerformed
        // TODO add your handling code here:
        
        control.mostrarLista( tblPessoas, ((JRadioButton) evt.getSource() ).getMnemonic() );
        
        
    }//GEN-LAST:event_rdbTipoActionPerformed

    public Pessoa getPessoaSelecionada() {
        return pesSelecionado;
    }
    
    /**
     * @param args the command line arguments
     */
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnPesquisar;
    private javax.swing.JButton btnSelecionar;
    private javax.swing.ButtonGroup grupoTipo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JRadioButton rdbPesFis;
    private javax.swing.JRadioButton rdbPesJur;
    private javax.swing.JRadioButton rdbTodos;
    private javax.swing.JRadioButton rdbVendedor;
    private javax.swing.JTable tblPessoas;
    private javax.swing.JTextField txtPesq;
    // End of variables declaration//GEN-END:variables
}
