/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import dao.*;
import java.util.Date;
import modelo.*;

/**
 *
 * @author 1547816
 */
public class Controlador {

    HerancaDAO dao;
    List<Pessoa> lista;

    //PedidoDAO pedDAO;
    public Controlador() throws ClassNotFoundException, SQLException, Exception {
        this.dao = new HerancaDAO();
        lista = null;
    }

    public int inserirPessoa(String nome, int tipo, String cpfcnpj, double sal, Date dtAdm)
            throws SQLException, Exception {
        
        Pessoa pes = null;
        if (tipo == 'F') {
            // PessoaFisica
            pes = new PessoaFisica(nome, cpfcnpj );
        } else if (tipo == 'J') {
            // PessoaJuridica
            pes = new PessoaJuridica(nome, cpfcnpj );
        } else {
            // Vendedor
            pes = new Vendedor(nome, cpfcnpj, sal, dtAdm );
        }
        dao.inserir(pes);
        return pes.getIdPessoa();

    }

    public void alterarPessoa(int idPessoa, String nome, int tipo, String cpfcnpj)
            throws SQLException, Exception {
        Pessoa pes;

        if (tipo == 'F') {
            // PessoaFisica
            pes = new PessoaFisica(nome, cpfcnpj );
        } else {
            // PessoaJuridica
            pes = new PessoaJuridica(nome, cpfcnpj );
        } 
        pes.setIdPessoa(idPessoa);

        dao.alterar(pes);

    }

    public void excluirPessoa(Pessoa pes, JTable tabela) throws SQLException, Exception {

        dao.excluir(pes);

        // Remover a linha selecionado
        int linha = tabela.getSelectedRow();
        ((DefaultTableModel) tabela.getModel()).removeRow(linha);
    }

    public void pesquisarPessoa(JTable tabela, String pesq, int tipo) {
        lista = dao.pesquisarNome(pesq, tipo);       
        mostrarLista(tabela,tipo);
    }
    
    public void mostrarLista(JTable tabela, int tipo) {
        String nomeClasse;
        Pessoa pes = null;
        
        switch ( tipo ) {
            case 'F': // Pessoa Fisica
                    nomeClasse = "modelo.PessoaFisica";
                    break;
            case 'J': // Pessoa Juridica
                    nomeClasse = "modelo.PessoaJuridica";
                    break;
            case 'V': // Pessoa Vendedor
                    nomeClasse = "modelo.Vendedor";
                    break;                
            default: 
                    nomeClasse = "todos";
                    break;
        }

        // Percorrer a LISTA
        if (lista != null) {
            ((DefaultTableModel) tabela.getModel()).setRowCount(0);
            Iterator<Pessoa> it = lista.iterator();
            while (it.hasNext()) {
                pes = it.next();
                
                if ( nomeClasse.equals( pes.getClass().getName() )  || nomeClasse.equals("todos") ) {
                    ((DefaultTableModel) tabela.getModel()).addRow(pes.toArray() );
                }                
            }
        }
        
    }

}
